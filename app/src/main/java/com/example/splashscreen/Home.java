package com.example.splashscreen_06;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

public class Home extends AppCompatActivity {
    private int waktu_loading = 400; //4000=4 detik

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_home);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                int home = new Intent(Home.this, Home.class);
                startActivity(Home);
                finish();
            }
        },waktu_loading);
    }
}
